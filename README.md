# README

Jalankan command berikut untuk running
```
docker build .
docker-compose up
```

Bila container web exit 
```
tmp/pids/server.pid 
```

Baca [disini](https://stackoverflow.com/questions/35022428/rails-server-is-still-running-in-a-new-opened-docker-container/35023225) 

Webpack error binstubs
```
bundle install --binstubs
```

Jalankan webpack
```
docker-compose run web ./bin/webpack
```
