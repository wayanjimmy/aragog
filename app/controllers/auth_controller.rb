class AuthController < ApplicationController
  layout :auth_layout

  def login
  end

  private
  def auth_layout
    'auth'
  end
end
